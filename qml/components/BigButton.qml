// BigButton.qml:

import QtQuick 2.0
import Sailfish.Silica 1.0

Rectangle {
    id: customButton
    width: parent.width
    height: Theme.itemSizeExtraLarge
    color: Theme.secondaryHighlightColor
    MouseArea {
        anchors.fill: parent
        onClicked: customButton.clicked()
    }

    signal clicked()

    property alias buttonText: buttonText

    Text {
        id: buttonText
        text: "Button Text"
        font.pixelSize: Theme.fontSizeExtraLarge
        color: Theme.primaryColor
        anchors.centerIn: parent
    }
}
