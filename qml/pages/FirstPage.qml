import QtQuick 2.0
import Sailfish.Silica 1.0
import QtQuick.Layouts 1.1
import "../components"

Page {
    id: page
    allowedOrientations: Orientation.All

    Image {
        id: backgroundImage
        source: "../images/sushi.jpg"
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Show Page 2")
                onClicked: pageStack.animatorPush(Qt.resolvedUrl("SecondPage.qml"))
            }
        }

        contentHeight: column.height

        PageHeader {
            title: qsTr("UI Template")
        }

        Column {
            id: column
            width: parent.width
            spacing: Theme.paddingLarge
            anchors.top: pageHeader.bottom
            anchors.horizontalCenter: parent.horizontalCenter

            Label {
                id: firstLabel
                width: parent.width
                text: qsTr("Argh")
                color: Theme.secondaryHighlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
                horizontalAlignment: Text.AlignHCenter
            }

            GridLayout {
                id: grid
                width: parent.width
                rows: 2
                columns: 2

                TextField {
                    id: field0
                    Layout.fillWidth: true
                    placeholderText: "Enter text"
                    font.pixelSize: Theme.fontSizeLarge
                    font.bold: true
                }

                TextField {
                    id: field1
                    Layout.fillWidth: true
                    placeholderText: "Enter text again"
                    font.pixelSize: Theme.fontSizeLarge
                    font.bold: true
                }

                TextField {
                    id: field2
                    Layout.fillWidth: true
                    placeholderText: "Enter text"
                    font.pixelSize: Theme.fontSizeLarge
                    font.bold: true
                }

                TextField {
                    id: field3
                    Layout.fillWidth: true
                    placeholderText: "Enter text again"
                    font.pixelSize: Theme.fontSizeLarge
                    font.bold: true
                }
            }

            Flow {
                width: parent.width
                spacing: Theme.paddingLarge

                Repeater {
                    model: 20

                    BigButton {
                        buttonText.text: "Button " + (index + 1)
                        onClicked: console.log("Button " + (index + 1) + " clicked!")
                    }
                }
            }

            Button {
                text: "QuickControl Button"
                onClicked: console.log("QuickControl button clicked")
            }
        }
    }
}
